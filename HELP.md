# Getting Started

### Create PostgreSQL database

```
shell> psql postgres

CREATE USER smsqueueing WITH
	LOGIN
	CONNECTION LIMIT -1
	PASSWORD 'smsqueueing';

CREATE DATABASE smsqueueing
    WITH 
    OWNER = smsqueueing
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;
```

