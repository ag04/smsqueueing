package com.ag04.jpaqueue;

import com.ag04.jpaqueue.retry.RetryPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.integration.leader.event.OnGrantedEvent;
import org.springframework.integration.leader.event.OnRevokedEvent;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.PreDestroy;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.*;

public class QueueConsumer {
	private final Logger logger = LoggerFactory.getLogger(QueueConsumer.class);

	private final TransactionTemplate transactionTemplate;

	private final QueueConsumerModule queueConsumerModule;
	private final RetryPolicy retryPolicy;
	private final long pollingPeriodInSecs;
	private final int itemsPollSize;

	private final ScheduledExecutorService scheduledExecutorService;
	private final int partitionCount;

	private Set<ScheduledFuture<?>> processingTasks;

	public QueueConsumer(
			QueueConsumerModule<?> queueConsumerModule,
			RetryPolicy retryPolicy,
			PlatformTransactionManager transactionManager,
			int polledItemsLimit,
			long pollingPeriodInSecs,
			int partitionCount
	) {
		if (polledItemsLimit < 1) {
			throw new IllegalArgumentException("Polled items size cannot be less than 1, bus is " + polledItemsLimit);
		}
		if (pollingPeriodInSecs < 1) {
			throw new IllegalArgumentException("Polling period cannot be less than 1, bus is " + polledItemsLimit);
		}
		if (partitionCount < 1) {
			throw new IllegalArgumentException("Partition count cannot be less than 1, but is " + partitionCount);
		}
		this.queueConsumerModule = Objects.requireNonNull(queueConsumerModule);
		this.retryPolicy = Objects.requireNonNull(retryPolicy);
		this.pollingPeriodInSecs = pollingPeriodInSecs;
		this.transactionTemplate = new TransactionTemplate(transactionManager);
		this.itemsPollSize = polledItemsLimit;
		this.partitionCount = partitionCount;
		this.scheduledExecutorService = Executors.newScheduledThreadPool(partitionCount);
	}

	@EventListener(OnGrantedEvent.class)
	public void onGrantedEvent() {
		logger.info("Granted leadership");

		startProcessingTasks();
	}

	private void startProcessingTasks() {
		logger.info("Starting {} queue polling tasks with delay of {} secs", partitionCount, pollingPeriodInSecs);

		Set<ScheduledFuture<?>> tasks = new HashSet<>();
		for (int i = 0; i < partitionCount; i++) {
			final int partition = i; // lambda requires 'final' variable, and loop requires it to be incremented, so we have to do it this way
			Runnable command = () -> processQueuedItems(partition);
			ScheduledFuture<?> task = this.scheduledExecutorService.scheduleWithFixedDelay(command, pollingPeriodInSecs, pollingPeriodInSecs, TimeUnit.SECONDS);
			tasks.add(task);
		}

		this.processingTasks = tasks;
	}

	@EventListener(OnRevokedEvent.class)
	public void onRevokedEvent() {
		logger.info("Revoked leadership");

		stopProcessingTasks();
	}

	private void stopProcessingTasks() {
		for (ScheduledFuture<?> task : processingTasks) {
			if (task != null && !task.isDone()) {
				task.cancel(true);
			}
		}
	}

	@PreDestroy
	public void destroy() throws Exception {
		this.scheduledExecutorService.shutdownNow();
	}

	public void processQueuedItems(int partition) {
		try {
			LocalDateTime now = LocalDateTime.now();
			List<?> itemIds = this.queueConsumerModule.findItemIdsWhereQueueingNextAttemptTimeIsBefore(partition, now, itemsPollSize);

			if (!itemIds.isEmpty()) {
				logger.info("Fetched {} pending queued items in partition {}", itemIds.size(), partition);
				for (Object itemId : itemIds) {
					processItemAndHandleErrorIfRaised(itemId);
				}
			}
		} catch (Throwable th) {
			logger.error("Error while fetching queued items: " + th.getMessage(), th);
		}
	}

	private void processItemAndHandleErrorIfRaised(Object itemId) {
		try {
			executeUnderTransaction(() -> processItem(itemId));
		} catch (Throwable error) {
			executeUnderTransaction(() -> registerProcessingFailure(itemId, error));
		}
	}

	private void executeUnderTransaction(Runnable runnable) {
		this.transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			protected void doInTransactionWithoutResult(TransactionStatus status) {
				runnable.run();
			}
		});
	}

	public void processItem(Object itemId) {
		Optional<QueueingState> queueingStateOptional = this.queueConsumerModule.processItem(itemId);
		if (queueingStateOptional.isPresent()) {
			queueingStateOptional.get().registerAttemptSuccess(LocalDateTime.now());
		} else {
			logger.warn("No queued item found under ID {} to process it", itemId);
		}
	}

	private void registerProcessingFailure(Object itemId, Throwable error) {
		logger.error("Error while processing item by ID " + itemId + ": " + error.getMessage(), error);

		Optional<QueueingState> queueingStateOptional = this.queueConsumerModule.getQueueingStateForItem(itemId);
		if (queueingStateOptional.isPresent()) {
			QueueingState queueingState = queueingStateOptional.get();
			queueingState.registerAttemptFailure(LocalDateTime.now(), error);

			Optional<LocalDateTime> retryAttemptTimeOptional = retryPolicy.calculateNextAttemptTime(queueingState.getLastAttemptTime(), queueingState.getAttemptCount());
			if (retryAttemptTimeOptional.isPresent()) {
				LocalDateTime nextAttemptTime = retryAttemptTimeOptional.get();
				logger.info("Retry for item by ID {} scheduled for time: {}", itemId, nextAttemptTime);
				queueingState.scheduleNextAttempt(nextAttemptTime);
			} else {
				logger.warn("No retry scheduled for item by ID {}", itemId);
			}
		} else {
			logger.warn("No queued item found under ID {} to register failed attempt", itemId);
		}
	}
}
