package com.ag04.smsqueueing;

import com.ag04.jpaqueue.QueueConsumer;
import com.ag04.jpaqueue.retry.FixedDelayRetryPolicy;
import com.ag04.jpaqueue.retry.LimitedRetryPolicy;
import com.ag04.jpaqueue.retry.RetryPolicy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.jdbc.lock.DefaultLockRepository;
import org.springframework.integration.jdbc.lock.JdbcLockRegistry;
import org.springframework.integration.jdbc.lock.LockRepository;
import org.springframework.integration.support.leader.LockRegistryLeaderInitiator;
import org.springframework.integration.support.locks.LockRegistry;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.time.Duration;

@SpringBootApplication
public class SmsQueueingApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmsQueueingApplication.class, args);
	}

	@Bean
	public QueueConsumer smsSendingQueueConsumer(SmsSendingQueueConsumerModule smsSendingQueueConsumerModule, PlatformTransactionManager transactionManager, @Value("${queueing.partition-count}") int partitionCount) {
//		RetryPolicy retryPolicy = new ExponentialDelayRetryPolicy(Duration.ofMinutes(1), 2);
		RetryPolicy retryPolicy = new LimitedRetryPolicy(3, new FixedDelayRetryPolicy(Duration.ofMinutes(1)));
		return new QueueConsumer(smsSendingQueueConsumerModule, retryPolicy, transactionManager, 100, 10, partitionCount);
	}

	@Bean
	public DefaultLockRepository lockRepository(DataSource dataSource) {
		return new DefaultLockRepository(dataSource);
	}

	@Bean
	public JdbcLockRegistry lockRegistry(LockRepository lockRepository) {
		return new JdbcLockRegistry(lockRepository);
	}

	@Bean
	public LockRegistryLeaderInitiator leaderInitiator(LockRegistry lockRegistry) {
		return new LockRegistryLeaderInitiator(lockRegistry);
	}

}
