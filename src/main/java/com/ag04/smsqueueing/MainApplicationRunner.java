package com.ag04.smsqueueing;

import com.ag04.smsqueueing.producer.SmsProducer;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class MainApplicationRunner implements ApplicationRunner {
	private final SmsProducer smsProducer;

	public MainApplicationRunner(SmsProducer smsProducer) {
		this.smsProducer = smsProducer;
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		Random rnd = new Random(System.currentTimeMillis());

		List<String> fromAddressesList = new ArrayList<>(fromAddresses());

		for (int i = 0; i < 70; i++) {
			String fromAddress = fromAddressesList.get(rnd.nextInt(fromAddressesList.size()));
			String toAddress = String.format("385913344%03d", rnd.nextInt(1000));
			LocalDateTime now = LocalDateTime.now();
			this.smsProducer.produce(fromAddress, toAddress, "Hello, this is text generated at " + now, now);
		}
	}

	public static Set<String> fromAddresses() {
		return Stream.of("80111", "80222", "80333", "80444", "80555")
				.collect(Collectors.toSet());
	}
}
