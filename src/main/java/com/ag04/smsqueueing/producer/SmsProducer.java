package com.ag04.smsqueueing.producer;

import java.time.LocalDateTime;

public interface SmsProducer {
	void produce(String fromAddress, String toAddress, String text, LocalDateTime sendTime);
}
