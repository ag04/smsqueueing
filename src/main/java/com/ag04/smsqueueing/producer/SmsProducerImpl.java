package com.ag04.smsqueueing.producer;

import com.ag04.smsqueueing.sms.SmsMessage;
import com.ag04.smsqueueing.sms.SmsMessageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
public class SmsProducerImpl implements SmsProducer {
	private final Logger logger = LoggerFactory.getLogger(SmsProducerImpl.class);

	private final SmsMessageRepository smsMessageRepository;
	private final int partitionCount;

	public SmsProducerImpl(SmsMessageRepository smsMessageRepository, @Value("${queueing.partition-count}") int partitionCount) {
		this.smsMessageRepository = smsMessageRepository;
		this.partitionCount = partitionCount;
	}

	@Override
	@Transactional
	public void produce(String fromAddress, String toAddress, String text, LocalDateTime sendTime) {
		logger.info("Producing SMS: fromAddress={}, toAddress={}, text={}, sendTime={}", fromAddress, toAddress, text, sendTime);

		String uid = UUID.randomUUID().toString();
		int partition = toAddress.hashCode() % this.partitionCount;
		SmsMessage smsMessage = new SmsMessage(uid, fromAddress, toAddress, text, partition);
		smsMessage.getSendingState().scheduleNextAttempt(sendTime);

		this.smsMessageRepository.save(smsMessage);
	}
}
