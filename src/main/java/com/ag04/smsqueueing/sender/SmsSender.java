package com.ag04.smsqueueing.sender;

import com.ag04.smsqueueing.sms.SmsMessage;

public interface SmsSender {
	void send(SmsMessage smsMessage);
}
