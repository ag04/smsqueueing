package com.ag04.smsqueueing.sender;

import com.ag04.smsqueueing.sms.SmsMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class SmsSenderImpl implements SmsSender {
	private static final Random RND = new Random(System.currentTimeMillis());

	private final Logger logger = LoggerFactory.getLogger(SmsSenderImpl.class);

	public SmsSenderImpl() {
	}

	@Override
	public void send(SmsMessage smsMessage) {
		logger.info("Sending SMS: " + smsMessage);
		if ((System.currentTimeMillis() % 4) == 0) {
			throw new IllegalStateException("Wrong time picked for send", new RuntimeException("I'm evil, that's the root cause!"));
		}
		fakeSendingDelay();
	}

	private void fakeSendingDelay() {
		try {
			Thread.sleep(50 + RND.nextInt(200));
		} catch (InterruptedException e) {
			throw new RuntimeException("Thread Interrupted");
		}
	}
}
