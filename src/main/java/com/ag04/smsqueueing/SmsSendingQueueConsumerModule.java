package com.ag04.smsqueueing;

import com.ag04.jpaqueue.QueueConsumerModule;
import com.ag04.jpaqueue.QueueingState;
import com.ag04.smsqueueing.sender.SmsSender;
import com.ag04.smsqueueing.sms.SmsMessage;
import com.ag04.smsqueueing.sms.SmsMessageRepository;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Component
public class SmsSendingQueueConsumerModule implements QueueConsumerModule<Long> {

	private final SmsMessageRepository smsMessageRepository;
	private final SmsSender smsSender;
	private final EntityManager entityManager;

	public SmsSendingQueueConsumerModule(SmsMessageRepository smsMessageRepository, SmsSender smsSender, EntityManager entityManager) {
		this.smsMessageRepository = smsMessageRepository;
		this.smsSender = smsSender;
		this.entityManager = entityManager;
	}

	@Override
	public List<Long> findItemIdsWhereQueueingNextAttemptTimeIsBefore(int partition, LocalDateTime time, int limit) {
		TypedQuery<Long> query = this.entityManager.createQuery(
				"select sms.id from com.ag04.smsqueueing.sms.SmsMessage sms where sms.partition = :partition and sms.sendingState.nextAttemptTime < :currentTime order by sms.sendingState.nextAttemptTime asc",
				Long.class);
		query.setParameter("partition", partition);
		query.setParameter("currentTime", time);
		query.setMaxResults(limit);
		return query.getResultList();
	}

	@Override
	public Optional<QueueingState> getQueueingStateForItem(Long itemId) {
		return this.smsMessageRepository.findById(itemId).map(SmsMessage::getSendingState);
	}

	@Override
	public Optional<QueueingState> processItem(Long itemId) {
		Optional<SmsMessage> smsOptional = this.smsMessageRepository.findById(itemId);
		if (smsOptional.isPresent()) {
			SmsMessage smsMessage = smsOptional.get();

			this.smsSender.send(smsMessage);

			return Optional.of(smsMessage.getSendingState());

		} else {
			return Optional.empty();
		}
	}
}
