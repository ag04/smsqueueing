package com.ag04.smsqueueing.sms;

import com.ag04.jpaqueue.QueueingState;

import javax.persistence.*;
import java.util.Objects;
import java.util.StringJoiner;

@Entity
@Table(name = "sms_message", indexes = @Index(name = "idx_sms_msg_queue_polling_fields", columnList = "next_attempt_time,partition"))
public class SmsMessage {
	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue
	private Long id;

	@Column(name = "uid", nullable = false, unique = true)
	private String uid; // app-assigned unique ID

	@Column(name = "from_address", nullable = false)
	private String fromAddress;

	@Column(name = "to_address", nullable = false)
	private String toAddress;

	@Column(name = "text", nullable = false)
	private String text;

	@Column(name = "partition")
	private int partition;

	private QueueingState sendingState = new QueueingState();

	private SmsMessage() {
		// for JPA
	}

	public SmsMessage(String uid, String fromAddress, String toAddress, String text, int partition) {
		this.uid = uid;
		this.fromAddress = fromAddress;
		this.toAddress = toAddress;
		this.text = text;
		this.partition = partition;
		this.sendingState = new QueueingState();

		if (partition < 0) {
			throw new IllegalArgumentException("Partition cannot be negative, but is: " + partition);
		}
	}

	public Long getId() {
		return id;
	}

	public String getUid() {
		return uid;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public String getToAddress() {
		return toAddress;
	}

	public String getText() {
		return text;
	}

	public int getPartition() {
		return partition;
	}

	public QueueingState getSendingState() {
		return sendingState;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof SmsMessage)) {
			return false;
		}
		SmsMessage smsMessage = (SmsMessage) o;
		return uid.equals(smsMessage.uid);
	}

	@Override
	public int hashCode() {
		return Objects.hash(uid);
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", SmsMessage.class.getSimpleName() + "[", "]")
				.add("id=" + id)
				.add("uid='" + uid + "'")
				.add("fromAddress='" + fromAddress + "'")
				.add("toAddress='" + toAddress + "'")
				.add("text='" + text + "'")
				.add("sendingState=" + sendingState)
				.add("partition=" + partition)
				.toString();
	}
}
